var express = require('express');
var router = express.Router();
const hbs = require('hbs');
var nodemailer = require('nodemailer');
var validate = require("validate.js");

hbs.registerHelper('list', function(items, options) {
  var out = "<ul>";

  for(var i=0, l=items.length; i<l; i++) {
    out = out + "<li>" + options.fn(items[i]) + "</li>";
  }

  return out + "</ul>";
});

/* GET contact page. */
router.get('/', function(req, res, next) {

  res.render('contact');
});

router.post('/', function(req, res, next) {
  //console.log(req.body);
  let contactdata = req.body;

  let validationConstraints = {
    from: {
      email: {
        message: "not valid email"
      }
    }
  }

  if(validate({from: contactdata.email}, validationConstraints) === undefined){
    const transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
            user: 'christodoulaki.kravmaga@gmail.com',
            pass: 'kravmag@1'
        }
    });

    let contactinfo = {
      from: contactdata.email, // sender address
      to: 'christodoulaki.kravmaga@gmail.com', // list of receivers
      subject: contactdata.subject, // Subject line
      html: `${contactdata.email}  with first name (${contactdata.fname}) and last name (${contactdata.lname}) SEND: ${contactdata.message}` // plain text body
    };
    
    transporter.sendMail(contactinfo, (err, info) => {
      if(err){
        res.send('NOT OK')
      }
      else {
      res.send('OK');
      }
    });

  }
  else {
    res.send('Not Valid Email');
  }
})

module.exports = router;

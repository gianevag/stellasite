var express = require('express');
var router = express.Router();
const hbs = require('hbs');

hbs.registerHelper('list', function(items, options) {
  var out = "<ul>";

  for(var i=0, l=items.length; i<l; i++) {
    out = out + "<li>" + options.fn(items[i]) + "</li>";
  }

  return out + "</ul>";
});

/* GET home page. */
router.get('/', function(req, res, next) {
  let a = {
    people: [
      {firstName: "Yehuda", lastName: "Katz"},
      {firstName: "Carl", lastName: "Lerche"},
      {firstName: "Alan", lastName: "Johnson"}
    ]
  };

  res.render('index', a);
});

/* GET home page. */
router.get('/contact', function(req, res, next) {

  res.render('contact');
});
module.exports = router;
